# Этап 1: Установка зависимостей
FROM python:3.9-alpine AS builder

RUN apk --no-cache add \
    build-base \
    libressl-dev \
    libffi-dev \
    python3-dev \
    py3-pip

WORKDIR /app

COPY requirements.txt /app/

RUN pip install --no-cache-dir -r requirements.txt \
    && pip install --no-cache-dir python-telegram-bot==13.15

# Этап 2: Создание финального образа
FROM python:3.9-alpine

WORKDIR /app

# Копирование установленных зависимостей из builder образа
COPY --from=builder /usr/local/lib/python3.9/site-packages/ /usr/local/lib/python3.9/site-packages/

# Копирование остальных файлов вашего приложения
COPY . /app/

EXPOSE 5000

CMD ["python", "app.py", "--port", "5000"]
