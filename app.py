from flask import Flask
from flask_cors import CORS
from app.routes import init_routes

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = 'uploads'

# Разрешаем CORS для всех доменов
CORS(app)
CORS(app, resources={r"/*": {"origins": ["https://c0d3r3v0lution.ru", "http://localhost:5173"]}})

init_routes(app)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
