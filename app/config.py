from dotenv import load_dotenv
import os

# Загрузка переменных из файла .env в окружение
load_dotenv()

# Теперь можно использовать переменные окружения
MONGODB_HOST = os.getenv('MONGODB_HOST')
MONGODB_PORT= os.getenv('MONGODB_PORT')
MONGODB_NAME = os.getenv('MONGODB_NAME')
RABBIT_HOST = os.getenv('RABBIT_HOST')
RABBIT_USER = os.getenv('RABBIT_USER')
RABBIT_PASSWORD = os.getenv('RABBIT_PASSWORD')
RABBIT_QUEUENAME = os.getenv('RABBIT_QUEUENAME')
