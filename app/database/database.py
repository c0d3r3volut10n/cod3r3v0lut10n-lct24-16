from pymongo import MongoClient

class Database:
    def __init__(self, uri, db_name):
        self.client = MongoClient(uri)
        self.db = self.client[db_name]
        self.collection = self.db['train_text']

    def insert_text(self, text):
        result = self.collection.insert_one({"text": text})
        return result.inserted_id

    def get_all_texts(self):
        texts = self.collection.find()
        return [{"id": str(text["_id"]), "text": text["text"]} for text in texts]

    def close_connection(self):
        self.client.close()