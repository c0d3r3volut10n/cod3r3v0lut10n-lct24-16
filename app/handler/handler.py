import csv
import logging
from io import StringIO
from app.database.database import Database
from app.rabbitMQ.rbbt import RabbitMQ
from app.config import MONGODB_HOST, MONGODB_PORT, MONGODB_NAME, RABBIT_QUEUENAME, RABBIT_HOST

# Initialize RabbitMQ connection
rabbit = RabbitMQ(host=RABBIT_HOST, queue_name=RABBIT_QUEUENAME)
rabbit.connect()

# Initialize MongoDB connection
db = Database(uri=f"mongodb://{MONGODB_HOST}:{MONGODB_PORT}/", db_name=MONGODB_NAME)


def process_csv(file_stream):
    # Read the CSV file
    csv_file = StringIO(file_stream.read().decode('utf-8'))
    reader = csv.reader(csv_file)

    # Skip the header row
    next(reader)

    # Extract the first column from each remaining row and store in MongoDB
    for row in reader:
        if row:
            text = row[0]  # Extract the first column
            inserted_id = db.insert_text(text)  # Вставляем текст и получаем его ID
            logging.info(f"Inserted text with ID: {inserted_id}")

            # Формируем сообщение для RabbitMQ
            message = {
                "id": str(inserted_id),  # Добавляем id записи в MongoDB
                "text": text
            }

            logging.debug(f"Sending message to RabbitMQ: {message}")
            rabbit.send_message(message)  # Отправляем сообщение в RabbitMQ





# Close connections when done
def close_connections():
    rabbit.close_connection()
    db.close_connection()
