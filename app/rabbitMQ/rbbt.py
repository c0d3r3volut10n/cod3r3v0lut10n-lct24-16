import json
import logging
import time

import pika
from pika.exceptions import AMQPConnectionError, AMQPChannelError
from app.config import RABBIT_HOST, RABBIT_QUEUENAME, RABBIT_USER, RABBIT_PASSWORD

class RabbitMQ:
    def __init__(self, host=RABBIT_HOST, queue_name=RABBIT_QUEUENAME, user=RABBIT_USER, password=RABBIT_PASSWORD):
        self.host = host
        self.queue_name = queue_name
        self.user = user
        self.password = password
        self.connection = None
        self.channel = None

    def connect(self, timeout=60):
        credentials = pika.PlainCredentials(self.user, self.password)
        parameters = pika.ConnectionParameters(self.host, 5672, '/', credentials)

        start_time = time.time()
        while True:
            try:
                self.connection = pika.BlockingConnection(parameters)
                break  # Успешное подключение, выходим из цикла
            except AMQPConnectionError as e:
                if time.time() - start_time > timeout:
                    logging.error(f"Failed to connect to RabbitMQ server within {timeout} seconds: {e}")
                    raise  # Превышен таймаут, выбрасываем исключение
                logging.warning(f"Connection to RabbitMQ failed: {e}, retrying...")
                time.sleep(1)  # Пауза перед повторной попыткой

        self.channel = self.connection.channel()
        self.channel.queue_declare(queue=self.queue_name, durable=True)

    def close_connection(self):
        if self.connection and not self.connection.is_closed:
            self.connection.close()

    def send_message(self, message):
        try:
            if not self.channel or self.channel.is_closed:
                self.connect()  # Повторно подключаемся, если канал был закрыт

            # Сериализуем весь объект сообщения в строку JSON с ensure_ascii=False
            message_body = json.dumps(message, ensure_ascii=False)

            # Публикуем сообщение в RabbitMQ
            self.channel.basic_publish(
                exchange='',
                routing_key=self.queue_name,
                body=message_body.encode('utf-8')
            )
            logging.info(f"Message sent to RabbitMQ: {message_body}")
        except AMQPConnectionError as e:
            logging.error(f"Error sending message to RabbitMQ - connection error: {e}")
        except AMQPChannelError as e:
            logging.error(f"Error sending message to RabbitMQ - channel error: {e}")
        except Exception as e:
            logging.error(f"Error sending message to RabbitMQ: {e}")

    def receive_message(self):
        method_frame, header_frame, body = self.channel.basic_get(self.queue_name)
        if method_frame:
            try:
                # Декодируем JSON и убеждаемся, что ensure_ascii=False
                message = json.loads(body.decode('utf-8'))
                logging.info(f"Message received from RabbitMQ: {message}")
                # Подтверждаем получение сообщения
                self.channel.basic_ack(method_frame.delivery_tag)
                return message
            except json.JSONDecodeError as e:
                logging.error(f"Error decoding JSON message: {e}")
                return None
        else:
            logging.info("No message returned")
            return None
