import logging

from bson import ObjectId
from flask import request, jsonify

from app.handler.handler import process_csv, rabbit, db


def init_routes(app):
    @app.route('/upload', methods=['POST'])
    def upload_post():
        if 'file' not in request.files:
            return jsonify({"error": "No file part"}), 400

        file = request.files['file']
        if file.filename == '':
            return jsonify({"error": "No selected file"}), 400

        if file and file.filename.endswith('.csv'):
            process_csv(file.stream)  # Передача потока файла для обработки в process_csv
            return jsonify({"message": "File processed and data inserted successfully"}), 200
        else:
            return jsonify({"error": "Invalid file type, only .csv allowed"}), 400

    @app.route('/results/<id>', methods=['PUT'])
    def update_result(id):
        data = request.json

        if not data:
            return jsonify({'error': 'No data provided'}), 400

        update_fields = {
            '$set': {
                'discount': data.get('discount'),
                'count_discount': data.get('count_discount')
            }
        }

        try:
            result = db.collection.update_one({'_id': ObjectId(id)}, update_fields, upsert=True)

            if result.matched_count == 1:
                return jsonify({'message': 'Document updated successfully'}), 200
            elif result.upserted_id is not None:
                return jsonify({'message': 'Document created successfully', 'id': str(result.upserted_id)}), 201
            else:
                return jsonify({'error': 'Failed to update or create document'}), 500

        except Exception as e:
            logging.error(f"Failed to update document: {e}")  # Логируйте исключение для отладки
            return jsonify({'error': str(e)}), 500

    @app.route('/message', methods=['POST'])
    def message_post():
        if request.is_json:
            data = request.get_json()
            text = data.get('text')
            collection_id = data.get('collection_id')
        else:
            text = request.form.get('text')
            collection_id = request.form.get('collection_id')

        if not text:
            logging.error("No text provided")
            return jsonify({"error": "No text provided"}), 400

        inserted_id = db.insert_text(text)
        logging.info(f"Inserted text with ID: {inserted_id}")

        if not inserted_id:
            logging.error("Failed to insert text into MongoDB")
            return jsonify({"error": "Failed to insert text into database"}), 500

        # Формируем сообщение для RabbitMQ
        message = {
            "id": str(inserted_id),  # Добавляем id записи в MongoDB
            "text": text
        }

        # Если collection_id был передан, добавляем его в сообщение
        if collection_id:
            message["collection_id"] = collection_id

        logging.debug(f"Sending message to RabbitMQ: {message}")
        rabbit.send_message(message)  # Отправляем сообщение в RabbitMQ

        return jsonify({"message": "Text received", "text": text, "id": str(inserted_id)}), 200

    @app.route('/results', methods=['GET'])
    def results():
        # Получаем все тексты из MongoDB
        texts = db.get_all_texts()
        return jsonify({"results": texts}), 200

    @app.route('/msg_rbbt', methods=['GET'])
    def msg_rbbt():
        msg = rabbit.receive_message()

        if msg is not None:
            return jsonify({"text": msg}), 200
        else:
            return jsonify({"message": "No messages available"}), 404
